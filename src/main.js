import Vue from 'vue';
// import App from './App.vue';
import MoneyClicker from './MoneyClicker.vue';

Vue.config.productionTip = false;

// new Vue({
//   render: h => h(App),
// }).$mount('#app');

// const params = {
//  render: h => h(App),
// };
// new Vue(params).$mount('#app');

new Vue({
  render: h => h(MoneyClicker),
}).$mount('#clicker-placeholder');
